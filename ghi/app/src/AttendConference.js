import React, { useEffect, useState } from 'react'

const AttendConference = () => {

    const fetchData = async () => {
        try{
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setConference(data.conferences)
        }
        else{
            console.error(response)
        }
    }catch(err){
        console.log(err)
    }finally{
        setLoading(false)
    }
    }
    useEffect(() => {
        fetchData();
      },[]);
      const [conference,setConference] = useState([])
      const [loading,setLoading]=useState(true)
      const[selectConference,setSelectConference]=useState('')
      const handleConferenceChange = (e)=>{
          const value = e.target.value
          setSelectConference(value); 
      }
      const [name, setName] = useState('');
      const handleNameChange = (e) => {
          const value = e.target.value;
          setName(value);
      }
      const [email, setEmail] = useState('');
      const handleEmailChange = (e) => {
          const value = e.target.value;
          setEmail(value);
      }
      const [sumbit,setSubmit]=useState(false)
      const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.email = email;
        data.name = name;
        data.conference=selectConference;
        console.log(data)
        const attendeesURL = "http://localhost:8001/api/attendees/";
        const fetchConfig = {
            method:'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
              },
        }
        const response = await fetch(attendeesURL, fetchConfig);
        if (response.ok) {
          const attendees = await response.json();
            console.log(attendees)
            setEmail('')
            setName('')
            setSelectConference('')
            setSubmit(true);
      }
      }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" alt=" " src="/logo.svg"/>
        </div>
        <div className="col">
          <div className="card shadow">
            <div id="card-body" className="card-body">
            {!sumbit &&
              <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                 {loading && <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>}
                </div>
                {!loading && 
                <div className="mb-3">
                <select onChange={handleConferenceChange} required id="location" value={selectConference} name="conference" className="form-select">
                <option value="">Choose a Conference</option>
                {conference.map(conference => {
                        return (
                        <option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                        );
                    })}
              </select>
                </div>
                    }
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleNameChange} value={name}required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input required onChange={handleEmailChange} value={email} placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
            }
              {sumbit && 
              <div className="alert alert-success mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
              }
            </div>
          </div>
        </div>
      </div>
  </div>
  )
}

export default AttendConference
