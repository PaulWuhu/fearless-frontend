import React, { useEffect, useState } from 'react'

const NewPresentation = () => {
    const [name, setName] = useState('');
    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }
    const [email, setEmail] = useState('');
    const handleEmailChange = (e) => {
        const value = e.target.value;
        setEmail(value);

    }
    const [companyName, setCompanyName] = useState('');
    const handleCompanyNameChange = (e) => {
        const value = e.target.value;
        setCompanyName(value);
    }
    const [title, setTitle] = useState('');
    const handleTitleChange = (e) => {
        const value = e.target.value;
        setTitle(value);
    }
    const [synopsis,setSynopsis]=useState('')
    const handleSynopsisChange= (e)=>{
      const value = e.target.value;
      setSynopsis(value);  
    }
    const[selectConference,setSelectConference]=useState('')
    const handleConferenceChange = (e)=>{
        const value = e.target.value
        setSelectConference(value); 
    }
    const [conference,setConference] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setConference(data.conferences)
        }
        else{
            console.error(response)
        }
    }
    useEffect(() => {
        fetchData();
      },[]);

      const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.title = title;
        data.presenter_email = email;
        data.presenter_name = name;
        data.synopsis = synopsis;
        data.company_name = companyName;
        let thisThing=selectConference;
        console.log(data)
        const presentationURL = `http://localhost:8000/api/conferences/${thisThing}/presentations/`;
        const fetchConfig = {
            method:'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
              },
        }
        const response = await fetch(presentationURL, fetchConfig);
        if (response.ok) {
          const presentation = await response.json();
            console.log(presentation)
            setCompanyName('')
            setEmail('')
            setSynopsis('')
            setTitle('')
            setName('')
            setSelectConference('')
      }
      }
  return (
    <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Presentation</h1>
          <form onSubmit={handleSubmit}id="create-presentation-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange}value={name} placeholder="Presenter Name" name="presenter_name" required type="text" id="name" className="form-control"/>
              <label htmlFor="name">Presenter Name</label>
            </div>
            <div className="form-floating mb-3">
              <input  onChange={handleEmailChange}value = {email} placeholder="Presenter Email" name="presenter_email" type="email" id="presenter_email" className="form-control"/>
              <label htmlFor="presenter_email">Presenter Email</label>
            </div>
            <div className="form-floating mb-3">
              <input value={companyName} onChange={handleCompanyNameChange} placeholder="Presenter Name" name="company_name" type="text" id="company_name" className="form-control"/>
              <label htmlFor="company_name">Company Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleTitleChange} value={title} placeholder="Title" name="title" required type="text" id="title" className="form-control"/>
              <label htmlFor="name">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="Synopsis">Synopsis</label>
              <textarea onChange={handleSynopsisChange} value={synopsis} name="synopsis"  required type="textarea" id="synopsis" style={{"height": "10rem"}}className="form-control min-h-3"></textarea>
            </div>
            <div className="mb-3">
              <select onChange={handleConferenceChange} required id="location" value={selectConference} name="conference" className="form-select">
                <option value="">Choose a Conference</option>
                {conference.map(conference => {
                        return (
                        <option key={conference.href} value={conference.id}>
                            {conference.name}
                        </option>
                        );
                    })}
              </select>
            </div>
            <button className="btn btn-primary" type="submit">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  )

}

export default NewPresentation
