import Nav from './nav';
import AttendeesList from './AttendeesList';
import { LocationForm } from './LocationForm';
import NewConference from './NewConference';
import NewPresentation from './newPresentation';
import AttendConference from './AttendConference';
import  MainPage  from './MainPage';
import { BrowserRouter as Router, Route , Routes } from 'react-router-dom'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Router>
      <Nav />
      <div className="container">
      <Routes>
      <Route index element={<MainPage />} />
        <Route exact path='/presentations/new' element={<NewPresentation/>}/>
        <Route exact path='/conferences/new' element={<NewConference />}/>
        <Route exact path='/attendees/new' element={<AttendConference/>}/>
        <Route exact path='/locations/new' element={<LocationForm/>}/>
        <Route exact path='/attendees' element={<AttendeesList attendees={props.attendees} />}/>
        <Route path='*' element={<h1> Nothing to see here </h1>}/>
      </Routes>
      </div>
      </Router>
    </>
  );
}

export default App;
