import React, { useEffect, useState } from 'react'
const NewConference = () => {
   const [locations,setLocations] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)
        }
        else{
            console.error(response)
        }
    }
    useEffect(() => {
        fetchData();
      },[]);
      const [name, setName] = useState('');
      const handleNameChange = (e) => {
          const value = e.target.value;
          setName(value);
      }
      const [start,setStart] = useState('')
      const handleStartChange = (e) => {
        const value = e.target.value;
        setStart(value);  
    }
    const [end,setEnd] = useState('')
    const handleEndChange = (e) => {
      const value = e.target.value;
      setEnd(value);  
  }
  const [description,setDescription]=useState('')
  const handleDescriptionChange= (e)=>{
    const value = e.target.value;
    setDescription(value);  
  }
  const [maxPresentations, setMaxPresentations] = useState(0);

  const handleMaxPresentations= (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  }
  const [maxAttendees, setMaxAttendees] = useState(0);

  const handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }
  const [selectLocation,setSelectLocation]=useState('')
  const handleSelectLocation = (e)=>{
    const value= e.target.value
    setSelectLocation(value)
  }
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.starts = start;
    data.ends = end;
    data.name = name;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees=maxAttendees;
    data.location=selectLocation;
    console.log(data)
    const conferenceURL = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
        method:'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
          },
    }
    const response = await fetch(conferenceURL, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
        console.log(newConference)
        setDescription('')
        setSelectLocation('')
        setMaxPresentations(0)
        setMaxAttendees(0)
        setStart('')
        setEnd('')
        setSelectLocation('')
        setName('')
    }
  }
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Conference</h1>
            <form onSubmit={handleSubmit}id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" name="name" required type="text" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} value={start}placeholder="mm/dd/yyyy" name="starts" required type="date" id="start-date" className="form-control"/>
                <label htmlFor="start-date">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} placeholder="mm/dd/yyyy" value={end} name="ends" required type="date" id="end-date" className="form-control"/>
                <label htmlFor="start-date">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} placeholder="Description Here" name="description" value={description} required type="textarea" id="description" style={{"height": "100px"}}className="form-control min-h-3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentations}placeholder="Maximum Presentations" name="max_presentations" required type="number" value={maxPresentations}id="Maximum_Presentations" className="form-control"/>
                <label htmlFor="Maximum_Presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange}placeholder="Maximum Attendees" name="max_attendees"value={maxAttendees} required type="number" id="Maximum_Attendees" className="form-control"/>
                <label htmlFor="Maximum_Attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleSelectLocation} value={selectLocation} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                        return (
                        <option key={location.href} value={location.id}>
                            {location.name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary" type="submit">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>

  )
}

export default NewConference
